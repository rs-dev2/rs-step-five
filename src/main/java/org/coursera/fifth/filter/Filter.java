package org.coursera.fifth.filter;

public interface Filter {
    boolean satisfies(String id);
}
