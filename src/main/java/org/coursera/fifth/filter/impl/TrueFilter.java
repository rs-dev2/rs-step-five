package org.coursera.fifth.filter.impl;

import org.coursera.fifth.filter.Filter;

public class TrueFilter implements Filter {
    @Override
    public boolean satisfies(String id) {
        return true;
    }
    
}
