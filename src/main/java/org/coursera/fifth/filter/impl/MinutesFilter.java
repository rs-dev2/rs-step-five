package org.coursera.fifth.filter.impl;

import org.coursera.fifth.db.MovieDatabase;
import org.coursera.fifth.filter.Filter;

public class MinutesFilter implements Filter {
    private final int min;
    private final int max;
    
    
    public MinutesFilter(int min, int max) {
        this.min = min;
        this.max = max;
    }
    
    @Override
    public boolean satisfies(String id) {
        return MovieDatabase.getMinutes(id) >= min && MovieDatabase.getMinutes(id) <= max;
    }
}
