package org.coursera.fifth.filter.impl;


import org.coursera.fifth.filter.Filter;

import java.util.ArrayList;
import java.util.List;

public class AllFilters implements Filter {
    List<Filter> filters;
    
    public AllFilters() {
        filters = new ArrayList<>();
    }
    
    public void addFilter(Filter filter) {
        filters.add(filter);
    }
    
    @Override
    public boolean satisfies(String id) {
        for (Filter filter : filters) {
            if (!filter.satisfies(id)) {
                return false;
            }
        }
        return true;
    }
    
}
