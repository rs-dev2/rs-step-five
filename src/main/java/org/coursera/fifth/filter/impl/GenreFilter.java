package org.coursera.fifth.filter.impl;

import org.coursera.fifth.db.MovieDatabase;
import org.coursera.fifth.filter.Filter;

public class GenreFilter implements Filter {
    private String genre;
    
    public GenreFilter(String genre) {
        this.genre = genre;
    }
    
    @Override
    public boolean satisfies(String id) {
        return MovieDatabase.getGenres(id).contains(genre);
    }
}
