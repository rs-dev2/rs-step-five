package org.coursera.fifth.recommender;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface Recommender {
    List<String> getItemsToRate() throws NoSuchAlgorithmException;

    void printRecommendationsFor(String webRaterId) throws NoSuchAlgorithmException;
}
