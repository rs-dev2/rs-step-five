package org.coursera.fifth;

import org.coursera.fifth.recommender.Recommender;
import org.coursera.fifth.recommender.impl.RecommendationImpl;

import java.security.NoSuchAlgorithmException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws NoSuchAlgorithmException {
        Recommender recommender = new RecommendationImpl();
        recommender.getItemsToRate();
        recommender.printRecommendationsFor("65");
    }
}
